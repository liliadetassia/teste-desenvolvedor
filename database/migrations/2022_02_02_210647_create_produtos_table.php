<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CLIENTE', function (Blueprint $table) {
            $table->id();
            $table->string('NomeCliente');
            $table->integer('CPF');
            $table->string('Email');
            $table->timestamps();
        });
    

    };

    {
        Schema::create('PRODUTOS', function (Blueprint $table) {
            $table->id();
            $table->integer('NumeroProduto');
            $table->string('NomeProduto');
            $table->integer('CodBarras');
            $table->decimal('ValorUnitario');
            $table->timestamps();
        });

    };  

    {
        Schema::create('PEDIDO', function (Blueprint $table) {
            $table->id();
            $table->integer('NumeroPedido');
            $table->integer('NumeroProduto');
            $table->integer('DtPedido');
            $table->intege('Quantidade');
            $table->timestamps();
        });
    };

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
