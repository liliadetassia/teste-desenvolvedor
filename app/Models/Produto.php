<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    use HasFactory;
}

class CLIENTE extends Model;
{
    protected $fillable= ['NomeCliente','CPF','Email'];
}

class PRODUTO extends Model;
{
    protected $fillable= ['NumeroProduto','NomeProduto','CodBarras','ValorUnitario'];
}

class PRODUTO extends Model;
{
    protected $fillable= ['NumeroPedido','NumeroProduto','DtPedido','Quantidade'];
}